import numpy as np
from matplotlib import pyplot as plt
import os
from scipy.optimize import fsolve, newton, fminbound, bisect
from scipy.integrate import odeint
import logging
import sys
import time
import pickle
from pycse import odelay

logging.basicConfig(filename='guderley_solver.log', level=logging.INFO)
logger = logging.getLogger('guderly solver')


class guderly_solver():

    def __init__(self, **kwargs):
        """
        kwargs is a dictionary with the following paramaters:gamma,density and mu
        """

        assert {'gamma', 'density', 'mu', 'geometry'} <= kwargs.keys()

        for key, value in kwargs.items():
            logger.info(f'{key} : {value}')
            setattr(self, key, value)

        if self.geometry == 'spherical':
            self.geometry = 3.
        elif self.geometry == 'cylindrical':
            self.geometry = 2.
        else:
            raise Exception('geometry can be either spherical or cylindrical')

        self.figure_out_state()
        self.lam = None

    def iterate_to_find_lambda_init(self, x, n=None, g=None, mu=None):

        if {n, g, mu} <= {None}:
            n = self.geometry
            g = self.gamma
            mu = self.mu

        Xx = (x / (x - 1.) + 2. * x / g) / (n - 1 + mu * x / g)
        alpha = Xx / (1. + Xx)
        V0 = alpha / x

        lam = (x - 1.) ** 2

        numer_eta = (2. * (1. - alpha) + (g - 1.) * mu * alpha)
        denom_eta = (n * alpha * g + mu * alpha - 2. * (1. - alpha))
        eta = numer_eta / denom_eta

        B = eta + (n - 1.) * (2. * lam + g - 1.) / (1. - n * lam)
        q = -x / (1. - n * lam)

        coeff_eta = (g - 1.) / ((g + 1.) * (1. - 1. / x))
        coeff_B = (V0 + q) / (2. * alpha / (g + 1.) + q)

        return lam - 0.5 * g * (g - 1.) * (coeff_eta ** eta) * (coeff_B ** B)

    def get_lambda_init(self, n=None, g=None, mu=None):

        if {n, g, mu} <= {None}:
            n = self.geometry
            g = self.gamma
            mu = self.mu

        x0 = np.sqrt(g * (g - 1.) / 2.) + 1.
        x = fsolve(self.iterate_to_find_lambda_init, x0, (n, g, mu))[0]
        Xx = (x / (x - 1.) + 2. * x / g) / (n - 1. + mu * x / g)
        lam = 1. / (Xx / (1. + Xx))
        return lam

    def figure_out_state(self):

        if self.mu <= -(self.geometry - 1):
            self.above_gamma_crit = True
            return self.above_gamma_crit

        if self.geometry == 3. and self.mu >= 4.:
            self.above_gamma_crit = False
        elif self.mu >= 2.:
            self.above_gamma_crit = False

        lam_init = self.get_lambda_init()

        g = self.gamma

        V0 = -2. / (g + 1.)

        C0 = np.sqrt(2 * g * (g - 1.)) / (g + 1.)

        Vspan = np.linspace(V0, 0., endpoint=False)

        V, C, VE, CE, IE = odelay(dCdV, C0, Vspan, events=[event1], args=(lam_init, self.gamma, self.geometry, self.mu))

        V_f = V[-1]
        C_f = C[-1]

        inter_section_points = calculate_intersection(lam_init, self.gamma, self.geometry, self.mu)

        dist_from_intersection = [
            (V_f - inter_section_points[0]) ** 2 + (C_f ** 2 - (inter_section_points[0] + 1.) ** 2) ** 2,
            (V_f - inter_section_points[1]) ** 2 + (C_f ** 2 - (inter_section_points[1] + 1.) ** 2) ** 2]

        if np.argmin(dist_from_intersection) == 0:
            self.above_gamma_crit = False

        else:
            self.above_gamma_crit = True

        return self.above_gamma_crit

    def iterate_to_find_lambda(self, lam, g, n, mu, change=False):
        V0 = -2. / (g + 1.)
        C0 = np.sqrt(2 * g * (g - 1.)) / (g + 1.)

        args = (lam, g, n, mu)

        if self.above_gamma_crit:
            index = 1
        else:
            index = 0

        V_inter = calculate_intersection(lam, g, n, mu)[index]
        C_inter = V_inter + 1.
        denom = 5e4 if self.above_gamma_crit else 5e7
        V_span = np.array([V0, V_inter - np.abs(V0 - V_inter) / denom, V_inter])

        C = odeint(dCdV, C0, V_span, args=args)

        L_anal = calculate_slopes(*args, self.above_gamma_crit)
        L_num = dCdV(C[-2], V_span[-2], *args)

        godel_C = np.log10(np.abs(C[-2] - C_inter))
        godel_L = np.log10(np.abs(L_anal - L_num))

        if self.above_gamma_crit:
            return -(L_anal - L_num) * 10. ** (5. + godel_C)

        logger.error(f'C = {C[-2]}, V_int = {V_inter:g}, V0 = {V0:g}, C0={C0:g}')
        return (C[-2] - C_inter) * 10. ** (5 + godel_L)

    def calc_lambda(self, lam_init=None, s_p=None):

        if lam_init is None:

            lam_init = self.get_lambda_init()

            if self.geometry == 3.:
                if self.mu < -1.7:
                    lam_init = 0.5
                    if self.mu < -2.8:
                        lam_init = 0.2
                else:
                    if self.mu < -0.5 and lam_init >= 1.5:
                        self.above_gamma_crit = True
                        lam_init = 0.9

            if self.geometry == 2.:
                if self.mu < -1.6:
                    lam_init = 0.3

        try:
            init_val = self.iterate_to_find_lambda(lam_init, self.gamma, self.geometry, self.mu)[0]
        except Exception:
            logger.error('exception_lam')
            lam_init = 0.6
            init_val = self.iterate_to_find_lambda(lam_init, self.gamma, self.geometry, self.mu)

        long_time = 5.
        logger.error(f'lam_init = {lam_init}')
        logger.error(f'init_val = {init_val}')
        logger.error(f'type = {type(init_val)}')

        if init_val >= 0:
            start_pos = True
        else:
            start_pos = False
        iter = 0

        if s_p is not None:
            start_pos = s_p

        while iter < 2:
            time_start = time.time()
            too_much_time = False
            if self.above_gamma_crit:
                if start_pos:
                    logger.error(f'1')
                    max_val = min_val = init_val
                    step = 0.05
                    min_lam = max_lam = lam_init
                    while True:
                        max_lam += step
                        between_lam = 0.5 * (max_lam + min_lam)
                        try:
                            max_val = self.iterate_to_find_lambda(max_lam, self.gamma, self.geometry, self.mu)
                        except Exception:
                            max_lam = between_lam
                            step = (max_lam - min_lam) / 10

                        if max_val * min_val < 0:
                            break


                        if time.time() - time_start > long_time:
                            too_much_time = True
                            break

                else:
                    logger.error(f'2')
                    max_lam = min_lam = lam_init
                    step = 0.05

                    max_val = min_val = init_val
                    while True:
                        min_lam -= step
                        min_val = self.iterate_to_find_lambda(min_lam, self.gamma, self.geometry, self.mu)

                        if min_val * max_val < 0:
                            break
                        if time.time() - time_start > long_time:
                            too_much_time = True
                            break


            else:
                if start_pos:
                    logger.error(f'3')
                    max_val = min_val = init_val
                    step = 0.03
                    max_lam = min_lam = lam_init

                    while True:
                        max_lam += step
                        between_lam = 0.5 * (max_lam + min_lam)
                        try:
                            max_val = self.iterate_to_find_lambda(max_lam, self.gamma, self.geometry, self.mu)
                        except Exception:
                            max_lam = between_lam
                            step = (max_lam - min_lam) / 10

                        if max_val * min_val < 0:
                            break

                        if time.time() - time_start > long_time:
                            too_much_time = True
                            break

                else:
                    logger.error(f'4')
                    max_val = min_val = init_val
                    step = 0.01
                    max_lam = min_lam = lam_init

                    while True:
                        min_lam -= step
                        min_val = self.iterate_to_find_lambda(min_lam, self.gamma, self.geometry, self.mu)

                        if max_val * min_val < 0:
                            break


                        if time.time() - time_start > long_time:
                            too_much_time = True
                            break

            if not too_much_time:
                break
            iter += 1
            start_pos = not start_pos

        if too_much_time:
            raise Exception


        self.lam = bisect(self.iterate_to_find_lambda, min_lam, max_lam, args=(self.gamma, self.geometry, self.mu))

        check_assert = np.abs(self.iterate_to_find_lambda(self.lam, self.gamma, self.geometry, self.mu, change=True))

        # assert check_assert < 1e-2, f'{check_assert}'

        return self

    def iterate_to_find_B(self, B):
        assert self.lam != None

        args = (self.lam, self.gamma, self.geometry, self.mu)

        lam, g, n, mu = args

        V0 = -2. / (g + 1.)
        C0 = np.sqrt(2 * g * (g - 1.)) / (g + 1.)
        R0 = (g + 1.) / (g - 1.)

        y0 = [V0, C0, R0]
        x_span = np.linspace(-1, B, endpoint=True)

        res_y = odeint(ode, y0, x_span, args=args)
        y_a = res_y[-1]

        self.V_arc_a = V_arc_a = (g - 1) * (1. + y_a[0]) / (g + 1.) + 2. * y_a[1] ** 2 / ((g + 1.) * (1 + y_a[0])) - 1.
        C_arc_a = np.sign(y_a[1]) * np.sqrt(y_a[1] ** 2 + (g - 1) / 2. * ((1. + y_a[0]) ** 2 - (1. + V_arc_a) ** 2))

        w0 = 1e-7

        V_beg_b = - (2. * (lam - 1.) - mu) / (n * g)
        sigma = (1. + (2. * (lam - 1.) + mu * (g - 1.)) / (2. * g * (1 + V_beg_b))) / lam

        y_beg_b = [V_beg_b, -1. / w0]

        w_span = np.linspace(w0, 1e5)

        w_res, y_res_b, wE, yE, IEB = odelay(ode_w, y_beg_b, w_span, events=[self.event_Vdiff], args=(*args, sigma))

        diff_C = np.diff(res_y[:, 1])
        C_arc_b = y_res_b[-1, 1]

        if not np.abs(V_arc_a - y_res_b[-1, 0]) > 1e-6 and not any(diff_C > 0):
            return C_arc_b - C_arc_a

        return np.nan

    def event_Vdiff(self, y, w):
        isterminal = True
        direction = 0
        value = self.V_arc_a - y[0]
        return value, isterminal, direction

    def calc_B(self):

        step = 2.
        B_min = 0.
        num_of_dev = 50

        fac = (self.gamma + 1.) / (self.gamma - 1.)

        B_max = None

        iter = 0

        while B_max is None:
            Bs = np.linspace(B_min, B_min + step, num_of_dev, endpoint=True)
            step /= num_of_dev
            for i, b in enumerate(Bs):
                val = self.iterate_to_find_B(fac * b)
                if val > 0:
                    B_min = b
                elif not np.isnan(val):
                    B_max = b
                if B_max is not None:
                    break
            iter += 1
            if iter == 3:
                self.B = np.nan
                return self

        B_min *= fac
        B_max *= fac

        self.B = bisect(self.iterate_to_find_B, B_min, B_max, xtol=1e-14, rtol=1e-14)
        return self

    def get_shock_velocity(self, t):
        return -1. / self.lam * (-t) ** (1. / self.lam - 1.)

    def get_shock_position(self, t):
        return (-t) ** (1. / self.lam)

    def solve(self, r, t):

        assert self.lam != None

        r = np.sort(np.append(r, (r[1:] + r[:-1]) / 2.))

        x_arr = t / r ** self.lam
        g = self.gamma
        n = self.geometry

        args = (self.lam, g, n, self.mu)

        inds_even = np.array([i % 2 == 0 for i in range(len(r))])
        inds_odd = np.logical_not(inds_even)

        density = np.empty_like(r)
        velocity = np.empty_like(r)
        cs_ = np.empty_like(r)

        if self.above_gamma_crit:
            index = 1
        else:
            index = 0

        if t < 0:
            logger.info('Solving Converging Shock')
            inds_after_shock = x_arr >= -1.
            inds_before_shock = x_arr < -1.

            x_arr_after_shock = x_arr[inds_after_shock]

            if -1. not in x_arr_after_shock:
                index_beggining_1 = 1
                x_arr_after_shock = np.concatenate([np.array([-1.]), x_arr_after_shock])

            else:
                index_beggining_1 = 0

            V0 = -2. / (g + 1.)
            C0 = np.sqrt(2 * g * (g - 1.)) / (g + 1.)
            R0 = (g + 1.) / (g - 1.)

            y0 = [V0, C0, R0]

            results_y = odeint(ode, y0, x_arr_after_shock, args=args)

            x_arr_after_shock = x_arr_after_shock[index_beggining_1:]
            results_y = results_y[index_beggining_1:, :]

            density[inds_after_shock] = results_y[:, 2] * self.density * r[inds_after_shock] ** self.mu
            velocity[inds_after_shock] = - r[inds_after_shock] / (t * self.lam) * results_y[:, 0]
            cs_[inds_after_shock] = -r[inds_after_shock] / (t * self.lam) * results_y[:, 1]

            density[inds_before_shock] = self.density * r[inds_before_shock] ** self.mu
            velocity[inds_before_shock] = 0.
            cs_[inds_before_shock] = 0.

            pressure = density * cs_ ** 2 / g
            energy = pressure / ((g - 1) * density)

            return {'density': density[inds_odd],
                    'velocity': velocity[inds_even],
                    'pressure': pressure[inds_odd],
                    'energy': energy[inds_odd],
                    'r': r[inds_even],
                    'r_avg': r[inds_odd],
                    'C': results_y[:, 1],
                    'V': results_y[:, 0],
                    'R': results_y[:, 2],
                    'x': x_arr}
        else:

            assert self.B is not None

            logger.info('Solving Diverging Shock')

            B = self.B

            inds_before_shock = x_arr < B
            inds_after_shock = x_arr >= B

            V0 = -2. / (g + 1.)
            C0 = np.sqrt(2 * g * (g - 1.)) / (g + 1.)
            R0 = (g + 1.) / (g - 1.)

            y0 = [V0, C0, R0]

            x_arr_before_shock = np.concatenate([[-1.], np.flip(x_arr[inds_before_shock]), [B]])
            x_arr_after_shock = np.concatenate([[B], np.flip(x_arr[inds_after_shock])])

            y_a = odeint(ode, y0, x_arr_before_shock, args=args)

            V_arc = (g - 1) * (1. + y_a[-1, 0]) / (g + 1.) + 2. * y_a[-1, 1] ** 2 / (
                    (g + 1.) * (1 + y_a[-1, 0])) - 1.

            C_arc = np.sign(y_a[-1, 1]) * np.sqrt(
                y_a[-1, 1] ** 2 + (g - 1) / 2. * ((1. + y_a[-1, 0]) ** 2 - (1. + V_arc) ** 2))

            R_arc = y_a[-1, 2] * (1. + y_a[-1, 0]) / (1. + V_arc)

            y_arc0 = [V_arc, C_arc, R_arc]

            y_a = y_a[1: -1]
            y_b = odeint(ode, y_arc0, x_arr_after_shock, args=args)[1:]

            y = np.flip(np.concatenate([y_a, y_b]), axis=0)

            density = y[:, 2] * self.density * r ** self.mu
            velocity = - r / (t * self.lam) * y[:, 0]
            cs_ = -r / (t * self.lam) * y[:, 1]
            pressure = density * cs_ ** 2 / g
            energy = pressure / ((g - 1) * density)
            
            imin_cell = 0
            for i in r[inds_odd] <= 0.05:
                if i:
                    imin_cell += 1
            
            imin_ver = 0
            for i in r[inds_even] <= 0.05:
                if i:
                    imin_ver += 1
            
            

            return {'density': density[inds_odd],
                    'velocity': velocity[inds_even],
                    'pressure': pressure[inds_odd],
                    'energy': energy[inds_odd],
                    'r': r[inds_even],
                    'r_avg': r[inds_odd],
                    'C': y[:, 1],
                    'V': y[:, 0],
                    'R': y[:, 2],
                    'x': x_arr,
                    'imin_cell' : imin_cell,
                    'imin_ver' : imin_ver}


def event5(y, x):
    global dist_to_0
    direction = 0
    isterminal = True
    value = y[0] ** 2 + y[1] ** 2 - dist_to_0
    return value, isterminal, direction


def event4(y, x):
    global shoolim
    direction = -1
    isterminal = True
    value = y[1] - (y[0] + 1. + shoolim)
    return value, isterminal, direction


def ode(y, x, lam, g, n, mu):
    V = y[0]
    C = y[1]
    D = lam * x * (C ** 2 - (V + 1.) ** 2)

    return [G(C, V, lam, g, n, mu) / D, F(C, V, lam, g, n, mu) / D, y[2] * H(C, V, lam, g, n, mu) / D]


def ode_w(y, w, lam, g, n, mu, sigma):
    V = y[0]
    C = y[1]
    D = -sigma * lam * w * (C ** 2 - (V + 1.) ** 2)

    return [G(C, V, lam, g, n, mu) / D, F(C, V, lam, g, n, mu) / D]


def F(C, V, lam, g, n, mu):
    C2 = C * C
    nu = n - 1

    return C * (C2 * (1. + (2 * (lam - 1.) + mu * (g - 1)) / (2 * g * (1. + V))) - (V + 1.) ** 2 - nu * (g - 1.) * (
            V * (1. + V)) / 2. - (
                        lam - 1.) * ((3. - g) * V + 2.) / 2.)


def G(C, V, lam, g, n, mu):
    C2 = C * C

    return C2 * (n * V + (2. * (lam - 1.) - mu) / g) - V * (V + 1.) * (lam + V)


def H(C, V, lam, g, n, mu):
    C2 = C * C
    return C2 * (-2. * (lam - 1.) + mu * (g * V + 1)) / (g * (V + 1.)) + V * (V + lam) - (n + mu) * V * (V + 1.)


def dCdV(C, V, lam, g, n, mu):
    return F(C, V, lam, g, n, mu) / G(C, V, lam, g, n, mu)


def dFdC(C, V, lam, g, n, mu):
    C2 = C * C
    nu = n - 1
    return 3. * C2 * (1. + (2 * (lam - 1.) + mu * (g - 1.)) / (2 * g * (1. + V))) - (1. + V) ** 2 - 0.5 * (
            2. + V * (3. - g)) * (
                   lam - 1.) - 0.5 * V * (1. + V) * (g - 1.) * nu


def dFdV(C, V, lam, g, n, mu):
    C2 = C * C
    nu = n - 1.

    return C * (-2. * (1. + V) - 0.5 * (3. - g) * (lam - 1.) - C2 * (2. * (lam - 1) + (g - 1) * mu) / (
            2 * g * (1 + V) ** 2) - 0.5 * V * (
                        g - 1.) * nu - 0.5 * (1 + V) * (g - 1.) * nu)


def dGdC(C, V, lam, g, n, mu):
    return 2 * C * ((2 * (lam - 1.) - mu) / g + V * n)


def dGdV(C, V, lam, g, n, mu):
    C2 = C * C
    return C2 * n - V * (1. + V) - V * (V + lam) - (1. + V) * (V + lam)


def calculate_intersection(lam, g, n, mu):
    nu = n - 1
    b = 1 + ((g - 2.) * (1. - lam) - mu) / (g * nu)
    c = (2. * (lam - 1.) - mu) / (g * nu)

    det = np.sqrt(b * b - 4 * c)

    if np.isnan(det):
        raise Exception

    return np.array([(-b - det) / 2., (-b + det) / 2.])


def event1(C, V):
    direction = 0
    isterminal = True
    value = C ** 2 - (V + 1.) ** 2

    return value, isterminal, direction


def calculate_slopes(lam, g, n, mu, above_gammacrit):
    inter_points = calculate_intersection(lam, g, n, mu)
    if above_gammacrit:
        V_int = inter_points[1]
    else:
        V_int = inter_points[0]

    coeff = 1.

    C_int = (V_int + 1.)

    Fc = dFdC(C_int, V_int, lam, g, n, mu)
    Gc = dGdC(C_int, V_int, lam, g, n, mu)
    Fv = dFdV(C_int, V_int, lam, g, n, mu)
    Gv = dGdV(C_int, V_int, lam, g, n, mu)

    R = np.sqrt((Fc - Gv) ** 2 + 4. * Fv * Gc)
    L = (Fc - Gv + coeff * R) / (2. * Gc)

    return L


if __name__ == '__main__':
    g_s = guderly_solver(gamma=1.5, mu=0., geometry='spherical', density=1.).calc_lambda().calc_B()
    r = np.linspace(0., 4., 500)
    sol = g_s.solve(r, 1)

    plt.plot(sol['r_avg'], sol['density'], '.-')
    plt.grid()
    plt.savefig('den.png')
    plt.close()
    plt.plot(sol['r_avg'], sol['pressure'], '.-')
    plt.grid()
    plt.savefig('pres.png')
    plt.close()
    plt.plot(sol['r'], sol['velocity'], '.-')
    plt.grid()
    plt.savefig('vel.png')
    plt.close()
